<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DrinksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "typeDrink_id"=>"required",
            "name"=>"required",
            "price"=>"required",
        ];
    }

    public function messages()
    {
        [
            "typeDrink_id.required"=>"typeDrink_id không được để trống",
            "name.required"=>"name đồ uống không được để trống",
            "price.required"=>"price đồ uống không được để trống"
        ];
    }
}