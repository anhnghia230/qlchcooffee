<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>"required",
            "email"=>"required|unique:users",
            "password"=>"required|min:6",
        ];
    }

    public function messages()
    {
     [
        "name.required"=>"name không được để trống",
        "email.required"=>"email không được để trống",
        "email.unique:users"=>"email đã tồn tại",
        "password.required"=>"password không được để trống",
        "password.min:6"=>"password phải lớn hơn 6 ký tự"
     ]; 
    }
}