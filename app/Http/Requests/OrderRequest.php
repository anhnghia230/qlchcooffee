<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nameDrinks"=>"required",
            "sl"=>"required",
            "nameCustomer"=>"required",
            "address"=>"required",
            "phone"=>"required",
        ];
    }

    public function messages()
    {
        [
            "nameDrinks.required"=>"tên đồ uống không được để trống",
            "sl.required"=>"số lượng đồ uống không được để trống",
            "nameCustomer.required"=>"tên khách hàng không được để trống",
            "address.required"=>"địa chỉ khách không được để trống",
            "phone.required"=>"số điện thoại không được để trống",
        ];
    }
}