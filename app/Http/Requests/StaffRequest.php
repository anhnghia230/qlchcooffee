<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id"=>"requỉed",
            "name"=>"required",
            "age"=>"required",
            "address"=>"required",
            "phone"=>"required",
            "cmnd"=>"required",
            "workingDay"=>"required",
            "position"=>"required",
            "img"=>"required|mimes:jpg,png|max:2048"
        ];
    }

    public function messages()
    {
       [
        "user_id.required"=>"user_id không được để trống",
        "name.required"=>"name không được để trống",
        "age.required"=>"age không được để trống",
        "address.required"=>"address không được để trống",
        "phone.required"=>"phone không được để trống",
        "cmnd.required"=>"cmnd không được để trống",
        "workingDay.required"=>"workingDay không được để trống",
        "position.required"=>"position không được để trống",
        "img.required"=>"img không được để trống",
       ];
    }
}