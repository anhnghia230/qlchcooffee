<?php

namespace App\Http\Controllers;

use App\Models\TypeDrink;
use Illuminate\Http\Request;

class TypeDrinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = TypeDrink::all();

        return $type;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $type = TypeDrink::create($request->all());

        return response()->json(["success:"=>"thêm loại đồ uống thành công","type:"=>$type],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeDrink  $typeDrink
     * @return \Illuminate\Http\Response
     */
    public function show(TypeDrink $typeDrink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeDrink  $typeDrink
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeDrink $typeDrink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeDrink  $typeDrink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = TypeDrink::find($id);
        $newType = $type->update($request->all());

        return response()->json(["success:"=>"update loại đồ uống thành công","type:"=>$newType],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeDrink  $typeDrink
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = TypeDrink::find($id);
        $type->delete();
        
        return response()->json(["success:"=>"xóa nhân viên thành công"],200);

    }
}