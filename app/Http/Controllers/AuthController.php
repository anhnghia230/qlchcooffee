<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    
    public function register(Request $request)
    {
        $user = User::create(array_merge(
            $request->all(),
            ["password"=>bcrypt($request->password)]
        ));

        return response()->json(["success:"=>"register thành công" , "user"=>$user],200);
    }

    public function login(Request $request)
    {
        return 1;
        dd(User::attempt($request->all()));
    }
}