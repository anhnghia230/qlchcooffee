<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DrinksController;
use App\Http\Controllers\OderController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\TypeDrinkController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("admin")->group(function()
{
    Route::post("/register",[AuthController::class,"register"]);
    Route::post("/login",[AuthController::class,"login"]);
});



Route::prefix("admin")->group(function()
{
    Route::get("/staff",[StaffController::class,"index"]);
    Route::post("/add/staff",[StaffController::class,"create"]);
    Route::put("/update/staff/{id}",[StaffController::class,"update"]);
    Route::delete("/delete/staff/{id}",[StaffController::class,"destroy"]);
});

Route::prefix("admin")->group(function()
{
    Route::get("/typeDrink",[TypeDrinkController::class,"index"]);
    Route::post("/add/typeDrink",[TypeDrinkController::class,"create"]);
    Route::put("/update/typeDrink/{id}",[TypeDrinkController::class,"update"]);
    Route::delete("/delete/typeDrink/{id}",[TypeDrinkController::class,"destroy"]);
});

Route::prefix("admin")->group(function()
{
    Route::get("/drinks",[DrinksController::class,"index"]);
    Route::post("/add/drinks",[DrinksController::class,"create"]);
    Route::put("/update/drinks/{id}",[DrinksController::class,"update"]);
    Route::delete("/delete/drinks/{id}",[DrinksController::class,"destroy"]);
});

Route::prefix("admin")->group(function()
{
    Route::get("/order",[OderController::class,"index"]);
    Route::delete("/delete/order/{id}",[OderController::class,"destroy"]);
});